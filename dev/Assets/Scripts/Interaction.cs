﻿using UnityEngine;

public class Interaction : MonoBehaviour
{
    [SerializeField]
    private TypeEnum m_Type;

    [SerializeField]
    private GameObject m_Prefab;

    public GameObject Prefab { get { return m_Prefab; } }


    private int m_PrimitiveCount;
    public int PrimitiveCount { get { return m_PrimitiveCount; } }



    void Awake()
    {
        m_PrimitiveCount = 0;
        
    }

    public void Start()
    {
        
    }

    void Update()
    {
        if (m_Type == TypeEnum.AnotherValue)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                new CubeCreator(m_PrimitiveCount);
                ++m_PrimitiveCount;
            }
            else if (Input.GetKeyDown(KeyCode.W))
            {
                new SphereCreator(m_PrimitiveCount);
                ++m_PrimitiveCount;
            }
            else if (Input.GetKeyDown(KeyCode.E))
            {
                new PlaneCreator(m_PrimitiveCount);
                ++m_PrimitiveCount;
            }
            else if (Input.GetKeyDown(KeyCode.R))
            {
                GameObject go = GameObject.Instantiate(m_Prefab);
                go.name = $"Prefab_{m_PrimitiveCount}";
                ++m_PrimitiveCount;
            }
        }
    }
}
